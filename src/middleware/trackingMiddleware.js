import { ADD_SCRIPT, TRACK_EVENT, TRACK_CUSTOM } from '../actions';

const trackingMiddleware = tracker => store => next => action => {
  if (action && action.type) {
    switch (action.type) {
      case ADD_SCRIPT:
        tracker.addScript(action.label, action.url);
        break;
      case TRACK_EVENT:
        tracker.trackEvent(action.event, action.argObj);
        break;
      case TRACK_CUSTOM:
        tracker.trackSingle(action.label, ...action.parameters);
        break;
    }
    next(action);
  }
};

export default trackingMiddleware;
